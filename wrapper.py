## Important Variables
window = None  # The main pyglet.window.Window object, defined in __init__
keystat = None # A dictionary of which pyglet.window.Keys are down
version = 0.2  # The game's current version number
connected = False # Wether or not the game is connected to a server
server_texture_dir = "resources/server" # The directory where resources downloaded
world_buffer = None # Also re-defined in __init__
view_hitboxes = False # If True, will display green circles by hitboxes

## Stack Calls
window_render_stack = []  # A list of rendering functions to call for windows
window_keycall_stack = [] # A list of functions to call when a key is pressed
window_mouse_stack= [] # A list of functions to call when a mouse button is pressed

## Settings
window_width =  1920
window_height = 1000
fullscreen = False

## Utilities Variables
punctuations = ".,/';<>?\":`~|{}[]()*&^%$#@!-_=+ "

## Common Utilities :

# Wether or not a character is punctuation (.!-,:' or other)
def is_punctuation(letter) :
    if letter in punctuations : return True
    else : return False

def hitcheck(mx, my, lx, ly, w, h) :
    if mx > lx and mx < lx + w : # Should use "not" and "or" instead of passing and then else.
        pass                     # It's like this because it's easier to add a debug message.
    else :                       # Also it works, so I'm not gonna fuark with it.
        return False

    if my > ly - h and my < ly :
        return True
    else :
        return False

# Takes a window and gives it calls for all stacks
def apply_fullwindow_call(window) :
    window_render_stack.append(window.draw)
    window_mouse_stack.append(window.mouse_press)
    window_keycall_stack.append(window.keypress)

# Does the above, but without the window object
def apply_fullwindow_calls(draw, mouse, key) :
    if not draw == None : window_render_stack.append(draw)
    if not mouse == None : window_mouse_stack.append(mouse)
    if not key == None : window_keycall_stack.append(key)

# Fullr remove a window from the window call stacks
def remove_fullwindow_calls(draw, mouse, key) :
    if not draw == None : window_render_stack.remove(draw)
    if not mouse == None : window_mouse_stack.remove(mouse)
    if not key == None : window_keycall_stack.remove(key)
    
