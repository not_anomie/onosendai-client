import subprocess

mime_command = "xdg-open"

def open_link(link) :
    subprocess.Popen([mime_command, link], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


