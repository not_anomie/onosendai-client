import pyglet
import threading
from pyglet.gl import *
import chat_gui
import console_gui
import player as player_controller
import pyglet.window.key as key
import node_types
import networking as n
import wrapper as w
from random import randrange
import resources as r

# Loads a to a specific name based off it's patb
def load_font(font_loc, fontname) :
    pyglet.font.add_file(font_loc)
    return pyglet.font.load(fontname)

# Makes the main game window
w.window = pyglet.window.Window(width=w.window_width, height=w.window_height, fullscreen=w.fullscreen)

w.window.set_minimum_size(w.window.width, w.window.height)
w.window.set_maximum_size(w.window.width, w.window.height)

# Make (0,0) the center of the window
glTranslatef(w.window.width/2.0, w.window.height/2.0, 0 )

# Load all the necessary fonts for the application to run
VT323 = load_font("resources/vt323.ttf", "VT323")

# Make the main world_buffer
w.world_buffer = node_types.world_buffer()

# Makes a label with the current build
version_label = pyglet.text.Label("CPO Ver: " + str(w.version), font_name="VT323")
version_label.x = -w.window.width/2 + 4
version_label.y = w.window.height/2 -14

# Adds the player node to the world buffer
w.world_buffer.add_node(player_controller)

@w.window.event
def on_draw() :
    w.window.clear()
    # Draws with world panning
    w.world_buffer.draw()
    # Draws at anchored points
    glTranslatef(player_controller.x, player_controller.y, 0)
    # Call all the window draw methods
    for i in w.window_render_stack :
        i()
    version_label.draw()

    glTranslatef(-player_controller.x, -player_controller.y, 0)

def outgoing_network_update(dt) :
    player_controller.update_server_position()
    n.get_nodes()
    n.recv_all_packets()

def update(dt) :
    player_controller.control_loop()

pyglet.clock.schedule_interval(update, 1/30.0)

@w.window.event
def on_mouse_press(x, y, button, mod) :
    for i in w.window_mouse_stack :
        if i(x,y,button) : break

@w.window.event
def on_mouse_release(x,y,button,mod) :
    console_gui.mouse_release(x, y, button)
    chat_gui.mouse_release(x,y,button)

@w.window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
    console_gui.on_mouse_drag(x, y, dx, dy, buttons, modifiers)
    chat_gui.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

@w.window.event
def on_key_press(k, mod) :
    # I'd rather this be in player with the other controls, but it has to return
    # to stop escape from closing the window
    if k == key.ESCAPE :
        chat_gui.unfocus()
        console_gui.unfocus()
        return pyglet.event.EVENT_HANDLED

    player_controller.on_key_press(k, mod)

@w.window.event
def on_key_release(k, mod) :
    player_controller.on_key_release(k, mod)

# Push creates keystate handler
w.keystat = pyglet.window.key.KeyStateHandler()
w.window.push_handlers(w.keystat)


pyglet.clock.schedule_interval(outgoing_network_update, 1/7.0)
pyglet.app.run()
