# Note that certain window types, such as the chat and console menus are defined as modules. As such, they can be found in their own files, rather than this one.
import pyglet
import wrapper as w
import resources as r
import pyglet.window.key as key

# A message box designed to support 109 characters.
class messagebox_109 :
    def __init__(self, msg="", x=0, y=0) :
        msg = msg[:109] # Chosen by graphically testing
        msg = self.split_message(msg, 19)
        y = -w.window_height/2.0 + 125 + r.msgbox.height/2.0
        self.label = pyglet.text.Label(
            text=msg,
            x=x-r.msgbox.width/2.0+15,
            y=y+r.msgbox.height/2.0 - 23,
            font_name="VT323",
            width=r.msgbox.width-15,
            multiline=True)
        self.label.color = [ 0, 0, 0, 0]
        self.sprite = pyglet.sprite.Sprite(img=r.msgbox, x=x, y=y)
        self.sprite.opacity = 0

    def split_message(self, msg, rows) :
        new_msg = ""
        for i in range(len(msg)) :
            if not i % rows == 0 or i == len(msg)-1 or i == 0:
                new_msg += msg[i]
                continue
            if i % rows == 0 :
                if w.is_punctuation(msg[i]) or w.is_punctuation(msg[i+1]) and not w.is_punctuation(msg[i-1]) :
                    if not msg[i-1] == " " : new_msg += msg[i] + "\n"
                    else : new_msg += "\n"
                    continue
                if not w.is_punctuation(msg[i]) and not w.is_punctuation(msg[i+1]) :
                    new_msg += msg[i] + "-\n"
                    continue
        return new_msg
            

    def mouse_press(self, x, y, button) :
        w.remove_fullwindow_calls(self.draw, None, None)
        del self # I don't *actually* know if this works...

    def draw(self) :
        # Because windows don't always get key calls...
        if w.keystat[key.SPACE] : self.mouse_press(1,2,3)
        if self.sprite.opacity < 255 : self.sprite.opacity += 15
        if self.label.color[3] < 255 :
            self.label.color = [ 0, 0, 0, self.sprite.opacity ]
        self.sprite.draw()
        self.label.draw()
