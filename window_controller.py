import chat_gui
import console_gui
import window_types
import wrapper as w

# A list of modules that represent windows that should be controlled
windows = [console_gui, chat_gui]

def set_layout_overlap() :
    # Find the highest Y value out of all the windows
    highest_dy = 0
    for i in windows :
        if hasattr(i, "default_y") :
            if i.background.height +4 > highest_dy : highest_dy = i.background.height + 4
    # Set the default positions of each window to overlap eachother
    for i in windows :
        if hasattr(i, "default_x") : i.default_x = 4
        if hasattr(i, "default_y") : i.default_y = highest_dy
    reset_windows("")

def set_layout_from_dict(layout_name) :
    for i in windows :
        if not hasattr(i, "layouts") : continue
        if not hasattr(i, "default_x") : continue
        if not hasattr(i, "default_y") : continue
        for k in i.layouts :
            if k == layout_name :
                i.default_x = i.layouts[layout_name][0]
                i.default_y = i.layouts[layout_name][1]
    reset_windows("")

# Resets the windows to their defaults, the extra 'cmd' and the return are to make it work in the console
# really I should wrap this function, but it's not going to be used *that* much outside of the console
def reset_windows(cmd) :
    for i in windows :
        if hasattr(i, "default_x") and hasattr(i, "default_y")  :
            i.move_window(i.default_x, i.default_y)
    return "Reset windows"

# Sends a message box with a short message in it
def send_msgbox(msg) :
    t = window_types.messagebox_109(msg=msg)
    w.window_render_stack.append(t.draw)
