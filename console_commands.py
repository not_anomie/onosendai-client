# CAREFUL, ANYTHING HERE CAN BE ACCESSED BY THE CONSOLE!!!
import window_controller
import window_types
import console_gui
import chat_gui
import networking as net
import wrapper
import MIMEHandling
import player
import pyglet
import resources
import os

def evaluate (cmd) :
    cmd = cmd[len("eval "):]
    return cmd + " = " + str(eval(cmd))

def execute (cmd) :
    cmd = cmd[len("exec "):]
    exec(cmd)
    return "Command executed: "

def run_bash(cmd) :
    cmd = cmd[len("run "):]
    os.system(cmd)
    return "Bash command run, success unkown."

def print_globals(cmd) :
    a = "Global Variables" + str( globals())
    return a

def set_window_layout(cmd) :
    cmd = cmd[len("set_window_layout "):]
    if cmd == "overlap" :
        window_controller.set_layout_overlap()
        return "Set window layout to " + str(cmd)
    if cmd == "cling_left" :
        window_controller.set_layout_from_dict("cling_left")
        return "Set window layout to " + str(cmd)
    if cmd == "overlap_center" :
        window_controller.set_layout_from_dict("overlap_center")
        return "Set window layout to " + str(cmd)

    return "\"" + cmd + "\" invalid window layout\nValid window layouts are" + str( [i for i in console_gui.layouts])

def set_name(cmd) :
    cmd = cmd[len("set_nick "):]
    net.server_sock.send("Set_Name|"+cmd+"\r\n")
    player.username = cmd[:len("Hellowor")] # I know it seems weird, but that's how many chars fit in the GUI. And I didn't want to count...
    chat_gui.name_label.text = "<" + player.username + ">"
    return "Tried to set name to " + cmd + ", you'll have to check the chat window for the results.."

def sim_packet(cmd) :
    cmd = cmd[len("sim_packet "):]
    net.parse_packet(cmd)
    return "Packet sent to loopback"

def clear_log(cmd) :
    console_gui.log = []
    console_gui.log_to_labels()
    return "Welcome to the CPO console v" + str(wrapper.version)

def open_link(cmd) :
    cmd = cmd[len("open "):]
    MIMEHandling.open_link(cmd)
    return "Opening " + str(cmd)


cmd_dict = {"eval":evaluate,
            "exec":execute,
            "run":run_bash,
            "globals":print_globals,
            "reset_windows":window_controller.reset_windows,
            "set_name":set_name,
            "sim_packet":sim_packet,
            "clear":clear_log,
            "open":open_link,
            "set_window_layout":set_window_layout
            }
