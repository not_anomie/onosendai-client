import networking as n
import wrapper as w
from pyglet.gl import *
import pyglet
import resources as r
import player

# Also not object orientated, this also might bite me in the ass later

# Logging and labeling
log = []
labels = []
label_batch = pyglet.graphics.Batch()
# Movement stuff
open = False
dragging = False
x = 4
default_x = 4
y = 4
default_y = 4
# Text rendering stuff
message_wrap_length = 38
input_len = 38
spacing = 21
max_messages = 11
# Window anchors
window_top = y + r.chat_window.height - 4
window_title= pyglet.text.Label(text="Chat Messages [T]:", font_name="VT323", x=x+5, y=window_top-10, color=(0,0,0,255))
background = pyglet.sprite.Sprite(r.chat_window)
name_label = pyglet.text.Label(text="<" + player.username + ">", font_name="VT323", x=x+17*r.general_scale, y=y+10, color=(0,0,0,255), anchor_x="center")
name_label.color = (0, 120, 0, 255)

# default layouts
layouts = {"cling_left":[4,4], "overlap_center":[w.window_width/2.0 - r.chat_window.width/2, 0]}

# Caret stuff
active_caret = False
doc = pyglet.text.document.UnformattedDocument(text="")
caret_label = pyglet.text.Label(text=doc.text, font_name="VT323", x=x+37*r.general_scale, y=y+10, color=(0,0,0,255))
layout = pyglet.text.layout.IncrementalTextLayout(doc, r.chat_window.width, 14)
caret = pyglet.text.caret.Caret(layout)

def make_caret() :
    if active_caret : return
    global active_caret
    active_caret = True
    pyglet.clock.schedule_once(w.window.push_handlers, 0.01, caret)
    bring_window_to_top()

def parse_message(msg) :
    split = split_message(msg)
    for i in split :
        log_message(i)

def split_message(msg) :
    # Filtering, should be it's own thing but whatever
    msg[1].replace("\\", "\\\\")
    msg[2].replace("\\", "\\\\")
    msgs = [msg[2][i:i + message_wrap_length] for i in range(0, len(msg[2]), message_wrap_length)]
    name_msgs = []
    for i in msgs :
        if i == msgs[0] :
            name_msgs.append( [msg[1], i])
        else :
            name_msgs.append( ["", " "+i])
    return name_msgs

def log_message(msg) :
    log.append( [msg[0], msg[1]])
    log_to_labels()

def log_to_labels() :
    global labels
    del labels
    labels = []
    for i in log[-max_messages:] :
        mk_label(i)

def mk_label(msg) :
    if len(labels) > max_messages : labels.remove(labels[0])
    labels.append(chat_message_label(msg[0], msg[1], y_off = (len(labels)+1)*spacing + 15) )

def draw() :
    # Un-center the window
    glTranslatef(-w.window.width/2.0, -w.window.height/2.0, 0 )
    background.x = x
    background.y = y
    background.draw()
    for i in labels :
        i.draw()
    update_caret_cursor()
    caret_label.draw()
    window_title.draw()
    name_label.draw()
    # Recenter the window
    glTranslatef(w.window.width/2.0, w.window.height/2.0, 0 )

def mouse_press(X, Y, button) :
    if w.hitcheck(X, Y, x, window_top, r.chat_window.width, 7*r.general_scale) :
        global dragging
        dragging = True
        bring_window_to_top()
        return True
    else : unfocus()
    return False

def mouse_release(x,y,button) :
    global dragging
    dragging = False

def on_mouse_drag(X, Y, dx, dy, buttons, modifiers):
    if not dragging : return
    global x
    x += dx
    global y
    y += dy
    global window_top
    window_top = y + r.chat_window.height - 4
    window_title.x += dx
    window_title.y += dy
    caret_label.x += dx
    caret_label.y += dy
    name_label.x += dx
    name_label.y += dy

def move_window(dx, dy) :
    dx =  dx - x
    dy =  dy - y
    global x
    x += dx
    global y
    y += dy
    global window_top
    window_top = y + r.chat_window.height - 4
    window_title.x += dx
    window_title.y += dy
    caret_label.x += dx
    caret_label.y += dy
    name_label.x += dx
    name_label.y += dy

class chat_message_label :
    def __init__(self, name, msg, y_off = 0 ):
        self.y_off = y_off
        self.name_label = pyglet.text.Label(text=str(name), font_name="VT323", batch=label_batch, anchor_x="center")
        self.name_label.color = (0, 120, 0, 255)
        self.name_label.x = x + 17 * r.general_scale
        self.name_label.y = window_top - y_off
        self.name_label.font_size=11

        self.msg_label = pyglet.text.Label(text=str(msg), font_name="VT323", batch=label_batch)
        self.msg_label.color = (0,0,0, 255)
        self.msg_label.x = x + 37*r.general_scale
        self.msg_label.y = window_top - y_off

    def refactor_location(self):
        self.msg_label.x = x + 37*r.general_scale
        self.msg_label.y = window_top - self.y_off

        self.name_label.x = x + 17 * r.general_scale
        self.name_label.y = window_top - self.y_off

    def draw(self):
        self.refactor_location()
        self.name_label.draw()
        self.msg_label.draw()

def send() :
    if not active_caret : return
    n.send_chat_packet(doc.text)
    global doc
    doc.text = ""
    unfocus()

def unfocus() :
    w.window.remove_handlers(caret)
    global active_caret
    active_caret = False

def bring_window_to_top() :
    w.remove_fullwindow_calls(draw, mouse_press, None)
    w.apply_fullwindow_calls(draw, mouse_press, None)

def update_caret_cursor() :
    new_label = doc.text[-input_len:]
    new_label = new_label[:caret.position]+"|"+new_label[caret.position:]
    if active_caret :
        caret_label.text= new_label
    else :
        caret_label.text= "" + doc.text[-input_len:]

w.apply_fullwindow_calls(draw, mouse_press, None)

