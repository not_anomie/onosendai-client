# It's not a class because there should only ever be one.
# This might bite me in the ass later...
import pyglet
import pyglet.window.key as key
from pyglet.gl import *
import networking as n
import chat_gui
import console_gui
import wrapper as w
import resources as r

# Player controls
keybinds = {
    "UP":key.UP,
    "DOWN":key.DOWN,
    "LEFT":key.LEFT,
    "RIGHT":key.RIGHT,
    "INTERACT":key.Z,
    "CONSOLE":key.GRAVE,
    "CHAT":key.T
    }

# Player data
x = 0
y = 0
z = 0
walk_speed= 4
sprint_speed = 6
speed = walk_speed
local_sprite = pyglet.sprite.Sprite(img=r.local_player, x=x, y=y)
node_id = -1
username = "Player"

# Cursor stuffs
cursor_distance = 50
cursor_sprite = pyglet.sprite.Sprite(img=r.selection_cursor, x=x, y=y)
cursor_sprite.opacity = 255/2.0

# Texture stuff
hold_texture= False
action_texture_dict = {"IDLE":0, "LEFT":1, "RIGHT":2, "UP":3, "DOWN":4}
current_animation = 0

def set_animation_range (offset) :
    count = 0
    for i in action_texture_dict :
        action_texture_dict[i] = offset+count
        count += 1

def set_animation(animation) :
    if hold_texture : return
    global current_animation
    current_animation = action_texture_dict[animation]
    if not local_sprite.image == r.texture_dict[ action_texture_dict[animation] ] :
        n.set_player_texture( action_texture_dict[animation] )

def hitbox_check (dx, dy) :
    for i in w.world_buffer.nodes :
        if not hasattr(i, "hitcheck") : continue
        if i.hitcheck(x+dx+(local_sprite.image.width-6)/2, y+dy-(local_sprite.image.height*0.25)) : return True
        if i.hitcheck(x+dx-(local_sprite.image.width-6)/2, y+dy-(local_sprite.image.height*0.25)) : return True
        if i.hitcheck(x+dx-(local_sprite.image.width-6)/2, y+dy-(local_sprite.image.height)/2) : return True
        if i.hitcheck(x+dx+(local_sprite.image.width-6)/2, y+dy-(local_sprite.image.height)/2) : return True
    return False

def keydown(name) :
    return w.keystat[ keybinds[name] ]

def move_up() :
    if hitbox_check(0, speed) : return
    global y
    y += speed
    local_sprite.y = y
    set_cursor_facing("UP")
    glTranslated(0, -speed, 0)

def move_down() :
    if hitbox_check(0, -speed) : return
    global y
    y -= speed
    local_sprite.y = y
    set_cursor_facing("DOWN")
    glTranslated(0, speed, 0)

def move_right() :
    if hitbox_check(speed, 0) : return
    global x
    x += speed
    local_sprite.x = x
    glTranslated(-speed, 0, 0)

def move_left() :
    if hitbox_check(-speed, 0) : return
    global x
    x -= speed
    local_sprite.x = x
    glTranslated(speed, 0, 0)

def control_loop() :
    global speed
    if chat_gui.active_caret or console_gui.active_caret : return
    if w.keystat[key.LSHIFT] :
        speed = sprint_speed
    else : speed = walk_speed

    if keydown("UP") :
        move_up()

    if keydown("DOWN") :
        move_down()

    if keydown("LEFT") :
        move_left()
        if not keydown("UP") and not keydown("DOWN") :
            set_animation("LEFT")
            set_cursor_facing("LEFT")
        else :
            set_cursor_facing("IDLE")

    if keydown("RIGHT") :
        move_right()
        if not w.keystat[key.UP] and not w.keystat[key.DOWN] :
            set_animation("RIGHT")
            set_cursor_facing("RIGHT")
        else :
            set_cursor_facing("IDLE")

def on_key_press(k, mod) :
    # Things that should work even when the player is typing
    if k == key.RETURN :
        chat_gui.send()
        console_gui.send()

    if k == key.PAGEUP :
        console_gui.scroll += 1
        console_gui.log_to_labels()
        print("SCROLL: " + str(console_gui.scroll) )

    if k == key.PAGEDOWN :
        console_gui.scroll -= 1
        console_gui.log_to_labels()
        print("SCROLL: " + str(console_gui.scroll) )

    if k == key.TAB :
        if console_gui.active_caret : console_gui.autocomplete_text()

    if k == keybinds["CONSOLE"] :
        if not chat_gui.active_caret :
            console_gui.make_caret()

    if k == keybinds["CHAT"] :
        if not console_gui.active_caret :
            chat_gui.make_caret()

    # Return if the player was typing during the keypress
    if console_gui.active_caret : return
    if chat_gui.active_caret : return

    # Things that shouldn't work when the player is typing
    if k == keybinds["UP"] :
        set_animation("UP")
        set_cursor_facing("UP")

    if k == keybinds["DOWN"] :
        set_animation("DOWN")
        set_cursor_facing("DOWN")
    
    if k == keybinds["INTERACT"] :
        interact_current()

def on_key_release(k, mod) :  ## TODO: This code needs to be split up
    # Return if the player was typing during the keypress
    if console_gui.active_caret : return
    if chat_gui.active_caret : return
    if w.keystat[key.UP] or w.keystat[key.DOWN] : return
    movement_keys = [key.LEFT, key.RIGHT, key.UP, key.DOWN]
    if not k in movement_keys : return
    if hold_texture : return
    n.set_player_texture(current_animation, moving=False)
    if hasattr(local_sprite.image, "frames") :
        local_sprite.image = local_sprite.image.frames[0].image

# Sort of belongs in networking, either that or n.set_player_texture belongs here.
def update_server_position() :
    pckt="Set_Player_Position|{lx}|{ly}\r\n".format(lx=x, ly=y)
    n.server_sock.send(pckt)

# Renders out everything in this module that needs rendering
def draw() : 
    local_sprite.draw()
    cursor_sprite.draw()

# Changes the location of the "interact with" cursor depending
# on the direction the player is facing
def set_cursor_facing(direction) :
    if direction == "IDLE" :
        cursor_sprite.x = local_sprite.x 
    if direction == "UP" :
        cursor_sprite.x = local_sprite.x 
        cursor_sprite.y = local_sprite.y + cursor_distance
    if direction == "DOWN" :
        cursor_sprite.x = local_sprite.x 
        cursor_sprite.y = local_sprite.y - cursor_distance
    if direction == "LEFT" :
        cursor_sprite.x = local_sprite.x - cursor_distance
        cursor_sprite.y = local_sprite.y        
    if direction == "RIGHT" :
        cursor_sprite.x = local_sprite.x + cursor_distance
        cursor_sprite.y = local_sprite.y        

def interact_current() :
    # Find nodes that the player could be referring to
    for i in w.world_buffer.nodes :
        if not hasattr(i, "sprite") : continue
        if w.hitcheck(
            cursor_sprite.x,
            cursor_sprite.y,
            i.sprite.x-i.sprite.image.width/2.0,
            i.sprite.y+i.sprite.image.height/2.0,
            i.sprite.image.width,
            i.sprite.image.height) and hasattr(i, "interact_call") :
                i.interact_call()
                return

