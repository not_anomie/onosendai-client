import pyglet
from copy import deepcopy
import resources as r
import networking as n
from pyglet.gl import *
import console_commands
import wrapper as w

# This one isn't object orientated for a reason,
# It should be used to launch single commands, and open new windows
# it should NOT ever be re-surped by one of the commands or programs.

# Message logging stuff
log = []
labels = []
label_batch = pyglet.graphics.Batch()
# Window movement stuff
open = False
dragging = False
x = 4
default_x = 4
y = 8 + r.console_window.height
default_y = 8 + r.console_window.height
# Text rendering stuff
message_wrap_length = 48
input_len = 48
spacing = 21
max_messages = 11
scroll = 0

# default layouts
layouts = {"cling_left":[4,8+r.console_window.height], "overlap_center":[w.window_width/2.0 - r.console_window.width/2, 0], "overlap":[0,0]} # overlap is only here for console printing

window_top = y + r.chat_window.height - 4
window_title= pyglet.text.Label(text="Console [~]:", font_name="VT323", x=x+5, y=window_top-10, color=(0,0,0,255))
background = pyglet.sprite.Sprite(r.console_window)

# Caret stuff
active_caret = False
doc = pyglet.text.document.UnformattedDocument(text="")
caret_label = pyglet.text.Label(text="> " + doc.text, font_name="VT323", x=x+4*r.general_scale, y=y+10, color=(0,255,0,255))
autocomplete_label = pyglet.text.Label(text="> ", font_name="VT323", x=x+4*r.general_scale, y=y+10, color=(100,100,100,255))
autocomplete_label.pending_autocomplete = ""
layout = pyglet.text.layout.IncrementalTextLayout(doc, r.chat_window.width, 14)
caret = pyglet.text.caret.Caret(layout)

def do_command(cmd) :
    for i in console_commands.cmd_dict :
        if i == cmd[:len(i)] :
            try :
                parse_message(console_commands.cmd_dict[i](cmd))
            except Exception as e: parse_message("Command failed: " + str(e))
            return
    parse_message("Command not found :/")

def make_caret() :
    if active_caret : return
    global active_caret
    active_caret = True
    pyglet.clock.schedule_once(w.window.push_handlers, 0.01, caret)
    bring_window_to_top()

def parse_message(msg) :
    msg = str(msg)
    split = split_message(msg)
    for i in split :
        log_message(i)

def split_message(msg) :
    # Filtering, should be it's own thing but whatever
    msgs = [msg[i:i + message_wrap_length] for i in range(0, len(msg), message_wrap_length)]
    return msgs

def log_message(msg) :
    log.append( msg)
    log_to_labels()

def log_to_labels() :
    global labels
    del labels
    labels = []

    global scroll
    if len(log) < max_messages :
        scroll = 0
        for i in log[-max_messages:] : mk_label(i)
    else :
        if scroll < 0 : scroll = 0
        if scroll+max_messages > len(log) : scroll = len(log) -max_messages
        for i in log[len(log)-max_messages-scroll: len(log)-scroll] :
            mk_label(i)


def mk_label(msg) :
    if len(labels) > max_messages : labels.remove(labels[0])
    labels.append(console_message_label(msg, y_off = (len(labels)+1)*spacing + 15) )

def check_autocomplete() :
    if len(doc.text) == 0 :
        autocomplete_label.text = ''
        return
    for i in console_commands.cmd_dict :
        if str(doc.text) in i and i[0] == str(doc.text)[0] :
            autocomplete_label.text = "> " + i
            autocomplete_label.pending_autocomplete = i
        else :
            continue

def autocomplete_text() :
    if doc.text == "" : return
    if autocomplete_label.pending_autocomplete == "" : return
    if autocomplete_label.pending_autocomplete in doc.text : return
    doc.text = autocomplete_label.pending_autocomplete
    caret.position = len(doc.text)

def draw() :
    # Un-center the window
    glTranslatef(-w.window.width/2.0, -w.window.height/2.0, 0 )
    background.x = x
    background.y = y
    background.draw()
    for i in labels :
        i.draw()
    update_caret_cursor()
    check_autocomplete()
    autocomplete_label.draw()
    caret_label.draw()
    window_title.draw()
    # Recenter the window
    glTranslatef(w.window.width/2.0, w.window.height/2.0, 0 )

def mouse_press(X, Y, button) :
    if w.hitcheck(X, Y, x, window_top, r.chat_window.width, 7*r.general_scale) :
        global dragging
        dragging = True
        bring_window_to_top()
        return True
    else : unfocus()
    return False

def mouse_release(x,y,button) :
    global dragging
    dragging = False

def on_mouse_drag(X, Y, dx, dy, buttons, modifiers):
    if not dragging : return
    global x
    x += dx
    global y
    y += dy
    global window_top
    window_top = y + r.chat_window.height - 4
    window_title.x += dx
    window_title.y += dy
    caret_label.x += dx
    caret_label.y += dy
    autocomplete_label.x += dx
    autocomplete_label.y += dy

def move_window(dx, dy) :
    dx =  dx - x
    dy =  dy - y
    global x
    x += dx
    global y
    y += dy
    global window_top
    window_top = y + r.chat_window.height - 4
    window_title.x += dx
    window_title.y += dy
    caret_label.x += dx
    caret_label.y += dy
    autocomplete_label.x += dx
    autocomplete_label.y += dy
    
class console_message_label :
    def __init__(self, msg, y_off = 0 ):
        self.msg_label = pyglet.text.Label(text=str(msg), font_name="VT323", batch=label_batch)
        self.msg_label.color = (0,255,30, 255)
        self.msg_label.x = x + 2*r.general_scale
        self.msg_label.y = window_top - y_off
        self.y_off = y_off

    def refactor_location(self):
        self.msg_label.x = x + 4*r.general_scale
        self.msg_label.y = window_top - self.y_off

    def draw(self):
        self.refactor_location()
        self.msg_label.draw()

def send() :
    if not active_caret : return
    global doc
    do_command(doc.text)
    doc.text = ""
    unfocus()
    global scroll
    scroll = 0

def unfocus() :
    w.window.remove_handlers(caret)
    global active_caret
    active_caret = False

def bring_window_to_top() :
    w.remove_fullwindow_calls(draw, mouse_press, None)
    w.apply_fullwindow_calls(draw, mouse_press, None)

def update_caret_cursor() :
    new_label = doc.text[-input_len:]
    new_label = new_label[:caret.position]+"|"+new_label[caret.position:]
    new_label = "> " + new_label
    if active_caret :
        caret_label.text= new_label
    else :
        caret_label.text= "> " + doc.text[-input_len:]

parse_message("Welcome to the CPO console v" + str(w.version))
w.apply_fullwindow_calls(draw, mouse_press, None)

