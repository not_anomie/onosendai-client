import pyglet
import console_gui
from pyglet.gl import *

pyglet.resource.path = ['./resources/']
pyglet.resource.reindex()

# scale factor for static sprites
general_scale = 3

# Animation bin
bin = pyglet.image.atlas.TextureBin()

# Texture dictionary
texture_dict = {}

# Wether or not the server is done sending textures
texture_dict_incomplete = True

# Indexes the resources directory
def reindex() :
    pyglet.resource.path = ['./resources']
    pyglet.resource.reindex()

# Puts an image in the texture dictionary from it's path
def add_text_to_dict (path, animation=False) :
    texture_count = -1
    for i in texture_dict :
        texture_count = i
    if animation : texture_dict[texture_count+1] = load_ani(path)
    if not animation : texture_dict[texture_count+1] = load_image(path)
    return texture_count + 1

# Prep a player animation
def process_ani(path) :
    animation = pyglet.image.load_animation(path)
    animation.scale = general_scale
    animation.add_to_texture_bin(bin)
    for i in animation.frames :
        animation.width = i.image.width * general_scale
        animation.height = i.image.height * general_scale
        i.image.width = i.image.width *general_scale
        i.image.height = i.image.height *general_scale
        i.width = i.image.width
        i.height = i.image.height
        i.image.anchor_x = i.image.width/2
        i.image.anchor_y = i.image.height/2
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    return animation

# Load an animation from a path
def load_ani(path) :
    print "Loading animation!"
    return process_ani("resources/" + path)

# Scale and center images where needed
def process_image(image) :
    image.width = image.width * general_scale
    image.height = image.height * general_scale
    image.anchor_x = image.width/2
    image.anchor_y = image.height/2

# Loads and image into a pyglet image object.
def load_image(image) :
    img = pyglet.resource.image(image)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    process_image(img)
    return img

chat_window = load_image("UI/ChatWindow.png")
chat_window.anchor_x = 0
chat_window.anchor_y = 0
console_window = load_image("UI/ConsoleWindow.png")
console_window.anchor_x = 0
console_window.anchor_y = 0
label_back = load_image("UI/Name_tag.png")
test_animation = load_ani("conf.gif")
default = load_image("default.png")
local_player = load_image("local_player.png")
selection_cursor = load_image("UI/InteractCrosshair.png")
msgbox = load_image("UI/MessageBox_Large.png")
