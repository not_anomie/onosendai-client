import socket
import sys
import console_gui
import chat_gui
import player as player_controller
import node_types
import resources as r
import wrapper as w

server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
not_closed = True
need_initial_nodes = True

def connect(addr="127.0.0.1", port=5000) :
    server_sock.connect((addr, port))
    w.connected = True

# Receives a texture over the TCP socket
def recv_image() :
    print("Waiting for image packet...")
    d = ''
    while True :
        d = d + str(server_sock.recv(1))
        if "<END_OF_PICTURE>\r\n" in d : break

    cont = ''
    while True :
        cont = cont + str(server_sock.recv(1))
        if "\r\n" in cont : break
    print (cont)

    if cont == "Not_Done_Sending_Textures\r\n" : pass
    if cont == "Done_Sending_Textures\r\n" : r.texture_dict_incomplete = False

    d_s = d.split("|")

    txtr_id = int(d_s[1])
    file_type = ""
    if d_s[2] == "gif" : file_type = ".gif"
    if d_s[2] == "png" : file_type = ".png"

    img_bin = '|'.join( d_s[3:] )
    opn = open( w.server_texture_dir+"/"+str(txtr_id)+file_type, 'w')
    opn.write(img_bin)
    opn.close()
    del img_bin  # Saves some major RAM dude
    r.reindex()
    print("Done!")
    return txtr_id, file_type

def recv_img_to_dict() :
    if not w.connected : return False
    id, file_type = recv_image()
    if file_type == ".png" :
        r.texture_dict[id] = r.load_image("server/" + str(id) + ".png")
    if file_type == ".gif" :
        r.texture_dict[id] = r.load_ani("server/" + str(id) + ".gif")
    return True

def recv_all_imgs() :
    server_sock.send("Get_Texture_Dictionary\r\n")
    while r.texture_dict_incomplete :
        recv_img_to_dict()
    print( r.texture_dict)

def update_node(node) :
    for i in range(len(w.world_buffer.nodes)) :
        if w.world_buffer.nodes[i].node_id == node.node_id :
            w.world_buffer.nodes[i] = node
            w.world_buffer.refactor_z_order()
            return
    print("Adding new node...")
    w.world_buffer.add_node(node)
    print("Done!")

def update_node_texture_safe(node) :
    for i in range(len(w.world_buffer.nodes)) :
        if w.world_buffer.nodes[i].node_id == node.node_id :
            if not w.world_buffer.nodes[i].texture_id == node.texture_id :
                w.world_buffer.nodes[i] = node
            w.world_buffer.nodes[i].x = node.x
            w.world_buffer.nodes[i].y = node.y
            w.world_buffer.nodes[i].z = node.z

            w.world_buffer.nodes[i].sprite.x = node.x
            w.world_buffer.nodes[i].sprite.y = node.y
            if hasattr(w.world_buffer.nodes[i], "name_tag") :
                w.world_buffer.nodes[i].name_tag.text = node.name_tag.text
            if hasattr(w.world_buffer.nodes[i], "moving") :
                w.world_buffer.nodes[i].moving = node.moving
            w.world_buffer.refactor_z_order()
            return
    print("Adding new node...")
    w.world_buffer.add_node(node)
    print("Done!")

def get_nodes() :
    if not need_initial_nodes :
        server_sock.send("Get_Node_Delta_Data\r\n")
    else :
        server_sock.send("Get_Node_Data\r\n")
        global need_initial_nodes
        need_initial_nodes = False


def get_messages() :
    server_sock.send("Get_Chat_Messages\r\n")

def recv_packet() :
    d = ''
    while True :
        d = d + str(server_sock.recv(1))
        if "\r\n" in d : break
    d_s = d.replace("\r\n", '').split("|")

    if d_s[0] == "Done_Sending_Packets" :
        return False
    parse_packet(d)
    return True

def parse_packet (d) :
    d_s = d.replace("\r\n", '').split("|")

    if d_s[0] == "Delete_Node" :
        for i in w.world_buffer.nodes :
            if i.node_id == int(d_s[1]) :
                w.world_buffer.nodes.remove(i)

    # These can be optimized by doing the ID check before the node creation
    if d_s[0] == "Decorative_Node" :
        n = node_types.decor_node(int(d_s[1]), int(d_s[2]), int(d_s[3]), int(d_s[4]), int(d_s[5]))
        update_node_texture_safe(n)

    if d_s[0] == "Player_Node" :
        n = node_types.remote_player(int(d_s[1]), int(d_s[2]), int(d_s[3]), int(d_s[4]), int(d_s[5]), str(d_s[6]), int(d_s[7]))
        update_node_texture_safe(n)

    if d_s[0] == "Hitbox_Node" :
        n = node_types.hitbox(  int(d_s[1]), int(d_s[2]), int(d_s[3]), int(d_s[4]), int(d_s[5]) )
        update_node(n)

    if d_s[0] == "Description_Node" :
        n = node_types.description_node(int(d_s[1]), int(d_s[2]), int(d_s[3]), int(d_s[4]), int(d_s[5]), str(d_s[6]))
        update_node_texture_safe(n)

    if d_s[0] == "Clear_Nodes" :
        w.world_buffer.clear_world()

    if d_s[0] == "Chat_Message" :
        chat_gui.parse_message(d_s)


def recv_all_packets() :
    server_sock.send("Get_Packet_Q\r\n")
    while recv_packet() : pass

def send_chat_packet(msg) :
    server_sock.send("Set_Chat_Message|" + msg + "\r\n")

def set_player_texture(id, moving=True) :
    player_controller.local_sprite.image = r.texture_dict[int(id)]
    server_sock.send("Set_Player_Texture|"+str(id)+"|"+str(int(moving))+"\r\n") # The str(int(moving)) thing turns False to "0" and True to "1"

ip = str(sys.argv[1])
port = int(sys.argv[2])
print("Using %a:%p".format(a=ip, p=port))

connect(addr=ip, port=port)
recv_all_imgs()
get_messages()

