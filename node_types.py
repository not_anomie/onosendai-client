import pyglet
import window_controller
import console_gui
import resources as r
import wrapper

class decor_node :
    def __init__(self, texture_id, x, y, z, node_id):
        self.texture_id = texture_id
        self.x, self.y, self.z = x, y, z
        self.node_id = node_id
        self.sprite = pyglet.sprite.Sprite(img=r.texture_dict[texture_id], x=self.x, y=self.y)

    def draw(self):
        self.sprite.draw()

class description_node :
    def __init__(self, texture_id, x, y, z, node_id, desc=''):
        self.desc = desc
        self.texture_id = texture_id
        self.x, self.y, self.z = x, y, z
        self.node_id = node_id
        self.sprite = pyglet.sprite.Sprite(img=r.texture_dict[texture_id], x=self.x, y=self.y)

    def draw(self):
        self.sprite.draw()

    def interact_call(self) :
        window_controller.send_msgbox(self.desc)

class hitbox :
    def __init__(self, x, y, w, h, node_id):
        self.x, self.y = x-(w/2), y+(h/2)
        self.w, self.h = w, h
        self.node_id = node_id

        if wrapper.view_hitboxes :
            self.labels = []
            self.labels.append( pyglet.text.Label(x=self.x, y=self.y, text="O", color=[0,255,0,255]))
            self.labels.append( pyglet.text.Label(x=self.x+w, y=self.y, text="O", color=[0,255,0,255]))
            self.labels.append( pyglet.text.Label(x=self.x, y=self.y-h, text="O", color=[0,255,0,255]))
            self.labels.append( pyglet.text.Label(x=self.x+w, y=self.y-h, text="O", color=[0,255,0,255]))

    def hitcheck(self, mx, my):
        return wrapper.hitcheck(mx, my, self.x, self.y, self.w, self.h)

    def draw(self):
        if not wrapper.view_hitboxes :
            return
        else :
            for i in self.labels : i.draw()

class remote_player :
    def __init__(self, texture_id, x, y, z, node_id, name, moving):
        self.moving = bool(moving)
        self.name_tag = pyglet.text.Label(text="<" + name + ">", x=x, y=y+25,  font_name="VT323", anchor_x="center")
        self.name_tag.color = [0,255,0,255]
        self.drop_shadow = pyglet.text.Label(text="<" + name + ">", x=x+1, y=y+23,  font_name="VT323", anchor_x="center", bold=True)
        self.drop_shadow.color = [0,0,0,255]
        self.texture_id = texture_id
        self.x, self.y, self.z = x, y, 0
        self.node_id = node_id
        self.sprite = pyglet.sprite.Sprite(img=r.texture_dict[texture_id], x=self.x, y=self.y)
        self.sprite.opacity = self.sprite.opacity*0.8

    def draw(self):
        if not self.z == 0 : print("NOT 0!!!")
        if hasattr(self.sprite.image, "frames") and not self.moving:
            self.sprite.image = self.sprite.image.frames[0].image
        if self.moving and not self.sprite.image == r.texture_dict[self.texture_id] :
            self.sprite.image = r.texture_dict[self.texture_id]
        self.name_tag.x = self.sprite.x
        self.name_tag.y = self.sprite.y + 25
        self.drop_shadow.x = self.sprite.x +1
        self.drop_shadow.y = self.sprite.y + 23
        self.sprite.draw()
        self.drop_shadow.draw()
        self.name_tag.draw()


class world_buffer :
    def __init__(self):
        self.nodes = []

    def draw(self) :
        for n in self.nodes :
            if hasattr(n, "draw") : n.draw()
        self.refactor_z_order()

    def clear_world(self):
        for i in self.nodes :
            if i.node_id == -1 : continue
            self.nodes.remove(i)

    def add_node(self, node):
        self.nodes.append(node)
        self.refactor_z_order()

    def refactor_z_order(self):
        self.nodes = sorted(self.nodes, key=lambda n: self.get_z_val(n), reverse=True)

    def get_z_val(self, node):
        if hasattr(node, "z") and hasattr(node, "y") :
            return node.z + node.y
        elif hasattr(node, "y") :
            return node.y
